window.addEventListener("load", function ()
{
   apiversion.value = location.href.match(/\d+\.\d+/);
   apiversion.addEventListener("change", function ()
   {
       location.href = location.href.replace(/\d+\.\d+/, apiversion.value);
   });
});
